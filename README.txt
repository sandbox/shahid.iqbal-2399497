Drupal VSG module:
------------------------
Maintainers:
  Shahid (https://drupal.org/user/824290)
Requires - Drupal 7
Dependency - Views

Overview:
--------
A views supported scattered gallery, where the items are scattered randomly
in a container.
You can add backface which will be shown by flipping when click on the current
navigation dot.


Features:
---------

* Use the power of Views style plugin
* Use views Rows style
* Random scattered gallery
* Support Backface (flip information)
* ON/OFF Auto Play
* Timer setting
* ON/OFF Navigation
* ON/OFF hover effect


Generally useful to create scattered gallery for:

* Images
* Portfolio
* Hot/Featured/On-Sale/Discount Products gallery
* Members profiles
* News / Blogs
* Any thing you like to display random using views


Installation:
------------
1. Download and unpack the Views_scattered_gallery module directory in your
   modules folder (this will usually be "sites/all/modules/").

2. Download and unpack the libraries files (classie.js, modernizr.min.js) in
   "Views_scattered_gallery/js/" folder.
    Make sure the path to the libraries file becomes:
    "Views_scattered_gallery/js/classie.js"
    "Views_scattered_gallery/js/modernizr.min.js"

Download Link:
--------------
http://tympanus.net/Development/ScatteredPolaroidsGallery/ScatteredPolaroidsGallery.zip


Configuration:
-------------
1. Create/update views (page or block) as usual.
2. formate: Select VSG style from views
3. Adjust VSG basic settings eg (Auto play, time delay, Navigation etc)
4. Add your required fields
5. Make sure "PAGER" settings should be "Display a specified number of items".
   other pager settings will not work with this style.
6. You have Done

How to add Backface:

7. Backface: Simply add the class "vsg-back" in your desired field
   using "STYLE SETTINGS => Customize field and label wrapper HTML"
   and this field will be use as backface.

Use the Views Field STYLE SETTINGS to add classes:
------------------------------------
TODO

Single field as backface:

   A quick way to add custom class in views
    i) Click on your desired field
    ii) select "STYLE SETTINGS"
    iii) select checkbox "Customize field and label wrapper HTML"
    iv) select checkbox "Create a CSS class"
    V) Add one or more classes seprated by space " "


Multiple fields as backface:

  Suppose you want three fields as backface, (title, body and category)
  1. Exclude these fields from display (title, body and category)
  2. Add new field "Global: Custom text"
  3. From "REPLACEMENT PATTERNS" add tokens for these three fields in text area.
     Such as,

       [title]
       [body]
       [category]


    i) select "STYLE SETTINGS"
    ii) select checkbox "Customize field and label wrapper HTML"
    iii) select checkbox "Create a CSS class"
    iV) Add one or more classes seprated by space " "

   4. Save views and you have done


Without back-face add Custom css to your theme:
----------------------------------------
If you are not using backface option for your VSG gallery, You can simple
add "width" property in your theme to adjust item view

.js .vsg figure {
  width: your_desired_size;
}
