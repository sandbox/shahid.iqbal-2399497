<?php
/**
 * @file
 * Default simple view template to display a VSG style rows.
 *
 * The variable $title : The title of this group of rows.
 * @ingroup views_templates
 */
?>

<?php print $vsg_wrapper_prefix; ?>
<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php print $vsg_content_prefix; ?>
<?php foreach ($rows as $id => $row): ?>
  <figure class="<?php print $classes_array[$id]; ?>"><?php print $row; ?></figure>
<?php endforeach; ?>
<?php print $vsg_content_suffix; ?>
<?php print $vsg_wrapper_suffix; ?>
