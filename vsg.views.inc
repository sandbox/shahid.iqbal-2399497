<?php

/**
 * @file
 * Defines the View Style Plugins for VSG module.
 */

/**
 * Implements hook_views_plugins().
 */
function vsg_views_plugins() {
  return array(
    'style' => array(
      'vsg' => array(
        'title' => t('VSG'),
        'help' => t('Display the views results as a scattered gallery.'),
        'handler' => 'VsgPluginStyleDisplay',
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'uses grouping' => FALSE,
        'uses row class' => TRUE,
        'type' => 'normal',
        'path' => drupal_get_path('module', 'vsg'),
        'theme' => 'vsg_view',
      ),
    ),
  );
}
