<?php

/**
 * @file
 * Contains the VSG style plugin.
 */

/**
 * Style plugin to render result as a scattered gallery.
 */
class VsgPluginStyleDisplay extends views_plugin_style {

  /**
   * Define options.
   *
   * @return array.
   *   Return form options array.
   */
  public function option_definition() {
    $options = parent::option_definition();
    // Define default classes.
    $options['vsg_wrapper_class'] = array('default' => 'vsg');
    $options['vsg_class'] = array('default' => 'vsg-content');
    // Define default values.
    $options['vsg_auto_play'] = array('default' => 1);
    $options['vsg_time_delay'] = array('default' => 3000);
    $options['vsg_stop_hover'] = array('default' => 1);
    $options['vsg_navigation'] = array('default' => 1);
    return $options;
  }

  /**
   * Render the given style.
   *
   * @param array $form
   *   In this parmater we are passing form array.
   * @param array $form_state
   *   In this parmater we are passing form posting data.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['vsg_wrapper_class'] = array(
      '#title' => t('Wrapper class'),
      '#description' => t('The class to provide on the wrapper, outside the VSG elements.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['vsg_wrapper_class'],
    );
    $form['vsg_class'] = array(
      '#title' => t('List class'),
      '#description' => t('The class to provide for the VSG elements.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['vsg_class'],
    );
    $form['vsg_auto_play'] = array(
      '#type' => 'checkbox',
      '#title' => t('Auto Play'),
      '#default_value' => $this->options['vsg_auto_play'],
      '#description' => t('If checked, VSG will auto play on start'),
    );
    $form['vsg_time_delay'] = array(
      '#title' => t('Time Delay'),
      '#description' => t('Set time delay for auto play VSG transaction. eg(1 second = 1000, 3 seconds = 3000'),
      '#type' => 'textfield',
      '#size' => 6,
      '#maxlength' => 6,
      '#default_value' => $this->options['vsg_time_delay'],
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[vsg_auto_play]"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['vsg_stop_hover'] = array(
      '#type' => 'checkbox',
      '#title' => t('Stop on hover'),
      '#default_value' => $this->options['vsg_stop_hover'],
      '#description' => t('Stop VSG animation effect on mouse over'),
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[vsg_auto_play]"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['vsg_navigation'] = array(
      '#type' => 'checkbox',
      '#title' => t('Navigation'),
      '#default_value' => $this->options['vsg_navigation'],
      '#description' => t('If checked, VSG navigation button will be display at bottom'),
    );
  }

  /**
   * Validate: Pager option should be disable for this style.
   *
   * @return array().
   *   Return form error array.
   */
  public function validate() {
    $errors = parent::validate();
    if ($this->display->handler->use_pager()) {
      $errors[] = t('The VSG style cannot be used with a pager. Disable the "Use pager" option for this display.');
    }
    if (empty($this->options['vsg_time_delay']) || !is_numeric($this->options['vsg_time_delay'])) {
      $errors[] = t('"Time Delay invalide value": Please provide valid number value. eg (3 second = 3000)');
    }
    return $errors;
  }

}
