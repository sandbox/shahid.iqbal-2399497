/**
 * @file
 * Provide js functions for VSG module.
 */

(function($) {
  Drupal.behaviors.vsg = {
    attach: function(context, settings) {
      // Drupal veriables.
      var auto_play = Drupal.settings.vsg.vsg_auto_play;
      var time_delay = Drupal.settings.vsg.vsg_time_delay;
      var stop_hover = Drupal.settings.vsg.vsg_stop_hover;
      // Define elements wrapper height.
      var oHeight = $('.vsg figure').outerHeight();
      oHeight = (oHeight * 2) - (oHeight / 2);
      $(".vsg .vsg-content").css("height", oHeight);

      // Add width to current SVG element.
      addwidth();

      /*
       * call vsg elements.
       * To batter view in views "live view display",
       * Should add navigation length condition to avoid duplication load.
       */
      $('.vsg').each(function(index, element) {
        new Photostack(element, {
          callback: function(item) {

          }
        });
      });
      // If VSG setting is auto play.
      if (auto_play == 1) {
        // Set timer.
        var i = 1;
        var timeInterval = setInterval(function() {
          // Call setNavInterval() function.
          setNavInterval()
        }, time_delay);
        // If setting is enable to stop on hover.
        if (stop_hover == 1) {
          $('.vsg nav span, .vsg-content').hover(function() {
            clearInterval(timeInterval);
          }, function() {
            timeInterval = setInterval(function() {
              // Call setNavInterval() function.
              setNavInterval()
            }, time_delay);
          });
          // End hover function.
        }
        // End if (stop_hover ==1)
      }
      // If( auto_play == 1).
      /*
       * Set VSG interval.
       */
      function setNavInterval() {
        var tSpan = $(".vsg nav span").length;
        // Add width to elements on auto play mode.
        addwidth();
        // Next element in VSG.
        if (i < tSpan) {
          $(".vsg nav span:eq(" + i + ")").click();
          i++;
        }
        /*
         * If it is last element
         * Start from first element
         **/
        else {
          i = 0;
        }
      } // End setNavInterval()

      /**
       * Function to calculate SVG element width
       * Add inline css width to element
       */
      function addwidth() {
        // Calculate SVG element width.
        var eWidth = $('.vsg .vsg-current dev:first').outerWidth();
        // Subtract padding size from top+bottom 25+25.
        $('.vsg .vsg-current').css("max-width", eWidth);
      }
    } // End drupal setting.
  }; // End drupal behaviour.
})(jQuery);
